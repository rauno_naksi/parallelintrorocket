#!/bin/bash
module purge
 # load a recent gcc compiler suite
module load openmpi-1.8.4

#go to home directory
cd $HOME
module load openmpi-1.8.4

export HPCG=hpcg
mkdir $HPCG
cd $HPCG

# get reference benchmark code
# check for more appropriate tuned versions
# at www.hpcg-benchmark.org

wget www.hpcg-benchmark.org/downloads/hpcg-3.0.tar.gz
tar -xvf hpcg-3.0.tar.gz
cd hpcg-3.0
# clean any possible old builds
make clean
# build MPI+OpenMP version
make arch=MPI_GCC_OMP
cd bin
# run code on two process with 20 threads per process
export OMP_NUM_THREADS=20
srun -t 00:10:00 --mem=20000  -N 2 mpirun xhpcg 32 32 32

